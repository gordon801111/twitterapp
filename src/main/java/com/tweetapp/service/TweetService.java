package com.tweetapp.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.tweetapp.domain.Tweet;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
 
public class TweetService {
	

	public List<Tweet> getAllTweet(String userid) {
		List<Tweet> tweets = new ArrayList<>();
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Filter keyFilter = new FilterPredicate("userid", Query.FilterOperator.EQUAL, userid);
		
		  
		Query query = new Query("Tweet").setFilter(keyFilter); 
		List<Entity> entities = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(100));
	    if(entities.isEmpty()) {
	    	return tweets;
	    }
		
		
	    for (Entity t: entities) {
	    	Tweet tweet = new Tweet();
	    	tweet.setText(t.getProperty("text").toString());
	    	tweet.setImage(t.getProperty("image").toString());
	    	tweet.setUserid(t.getProperty("userid").toString());
	    	tweet.setUsername(t.getProperty("username").toString());
	    	tweet.setViews((int)(long)t.getProperty("views"));
	    	tweet.setCreatedAt(t.getProperty("createdAt").toString());
	    	tweet.setId(KeyFactory.keyToString(t.getKey()));
	    	tweets.add(tweet);
	    }
	    
	    return tweets;
	}
	
	
	
	
	public Tweet getOneTweet(String id) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.stringToKey(id);
		Tweet tweet = new Tweet();
		
		try{
			Entity t = datastore.get(key);
	    	tweet.setText(t.getProperty("text").toString());
	    	tweet.setImage(t.getProperty("image").toString());
	    	tweet.setUserid(t.getProperty("userid").toString());
	    	tweet.setUsername(t.getProperty("username").toString());
	    	tweet.setViews((int)(long)t.getProperty("views"));
	    	tweet.setCreatedAt(t.getProperty("createdAt").toString());
	    	tweet.setId(KeyFactory.keyToString(t.getKey()));
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		 
	    return tweet;
	}
	
	public void updateView(String id) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.stringToKey(id); 
		
		try{
			Entity t = datastore.get(key);
			t.setProperty("views", (int)(long) t.getProperty("views") + 1);
			datastore.put(t);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	 
	
	public List<Tweet> getTopTweets() {
		List<Tweet> tweets = new ArrayList<>();
		
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService(); 
		Query query = new Query("Tweet");
		query.addSort("views", Query.SortDirection.DESCENDING); 
	    PreparedQuery preparedquery = datastore.prepare(query);
	    for (Entity t: preparedquery.asIterable()) {
	    	Tweet tweet = new Tweet();
	    	tweet.setText(t.getProperty("text").toString());
	    	tweet.setImage(t.getProperty("image").toString());
	    	tweet.setUserid(t.getProperty("userid").toString());
	    	tweet.setUsername(t.getProperty("username").toString());
	    	tweet.setViews((int)(long)t.getProperty("views"));
	    	tweet.setCreatedAt(t.getProperty("createdAt").toString());
	    	tweet.setId(KeyFactory.keyToString(t.getKey()));
	    	tweets.add(tweet);
	    }
	    
	    return tweets;
	}
	
	
	
	public List<Tweet> getFriendsTweets(String userid) {
		List<Tweet> tweets = new ArrayList<>();
		
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService(); 
		Filter keyFilter = new FilterPredicate("userid", Query.FilterOperator.NOT_EQUAL, userid);
		Query query = new Query("Tweet").setFilter(keyFilter); 
	    PreparedQuery preparedquery = datastore.prepare(query);
	    for (Entity t: preparedquery.asIterable()) {
	    	Tweet tweet = new Tweet();
	    	tweet.setText(t.getProperty("text").toString());
	    	tweet.setImage(t.getProperty("image").toString());
	    	tweet.setUserid(t.getProperty("userid").toString());
	    	tweet.setUsername(t.getProperty("username").toString());
	    	tweet.setViews((int)(long)t.getProperty("views"));
	    	tweet.setCreatedAt(t.getProperty("createdAt").toString());
	    	tweet.setId(KeyFactory.keyToString(t.getKey()));
	    	tweets.add(tweet);
	    }
	    
	    return tweets;
	}
	
	
	public void createTweet(Tweet tweet) { 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity entity = new Entity("Tweet");
		entity.setProperty("text", tweet.getText());
		entity.setProperty("image", "//graph.facebook.com/" + tweet.getUserid() + "/picture?type=normal");
		entity.setProperty("views", tweet.getViews());
		entity.setProperty("userid", tweet.getUserid());
		entity.setProperty("username", tweet.getUsername());
		entity.setProperty("createdAt", new Date());
		datastore.put(entity);
	}
	
	public void deleteTweet(String id) { 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.stringToKey(id);  
		datastore.delete(key);
	}
	 
	
	public void TweetSeed() { 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		for (int i = 0; i < 10; i++) {
			Entity entity = new Entity("Tweet");
			entity.setProperty("text", "This is tweet no." + String.valueOf(i));
			entity.setProperty("image", "none");
			entity.setProperty("views", i);
			entity.setProperty("userid", String.valueOf(i));
			entity.setProperty("username", "Gordon");
			entity.setProperty("createdAt", new Date());
			datastore.put(entity);
		}
		
	}
	
}
