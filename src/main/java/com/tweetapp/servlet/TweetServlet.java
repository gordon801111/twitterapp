package com.tweetapp.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tweetapp.domain.Tweet;
import com.tweetapp.service.TweetService;

@WebServlet("/TweetServlet")

public class TweetServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = "/index.jsp";
		String action = request.getParameter("action");
		String source = request.getParameter("source");
		try{
			if (action.equals("display")) { 
				TweetService ts = new TweetService();
				try{
					Tweet tweet = ts.getOneTweet((String) request.getParameter("id"));
					request.setAttribute("tweet", tweet);
					
					if(source.equals("friendpage")) {
						ts.updateView((String) request.getParameter("id"));
					}
					
					
					url = "/tweet.jsp";
				} catch (Exception ex) {
					url = "/index.jsp";
				}
			} else {
				String userid = request.getParameter("userid");
				String username = request.getParameter("username");
				HttpSession session = request.getSession();
				session.setAttribute("userid", userid);
				session.setAttribute("username", username);
			}
		} catch (Exception ex){
			ex.printStackTrace();
		}
		 
		getServletContext().getRequestDispatcher(url).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = "/index.jsp";
		HttpSession session = request.getSession();

		String action = request.getParameter("action");
		
		try{
			if (action.equals("create")) {
				Tweet tweet = new Tweet();
				tweet.setText(request.getParameter("text"));
				tweet.setUserid(String.valueOf(session.getAttribute("userid")));
				tweet.setUsername(String.valueOf(session.getAttribute("username")));
				TweetService ts = new TweetService();
				ts.createTweet(tweet);
			} else if (action.equals("delete")) {
				TweetService ts = new TweetService();
				ts.deleteTweet((String) request.getParameter("id"));
			}
		} catch (Exception ex){
			ex.printStackTrace();
		}
		 
		getServletContext().getRequestDispatcher(url).forward(request, response);
	}

}
