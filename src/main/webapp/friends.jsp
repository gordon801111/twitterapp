<%@page import="com.tweetapp.service.TweetService"%>
<%@page import="java.util.List"%>
<%@page import="com.tweetapp.domain.Tweet"%>
<%@ include file="/includes/header.jsp"%>
<fb:login-button id="pleaselogin"
				scope="public_profile,email,user_birthday,user_location,user_posts"
				onlogin="checkLoginState();">
			</fb:login-button> 

<div id="fb-welcome"> </div>
<div id="friendpage">
	<%
		TweetService ts = new TweetService();
		List<Tweet> tweets = ts.getFriendsTweets((String) session.getAttribute("userid"));
		request.setAttribute("tweets", tweets);
	%>


	<div>
		<c:forEach var="tweet" items="${tweets}">
			<div class="feed">
				<div class="media">
					<img class="align-self-start mr-3"
						src="${tweet.image}"
						alt="Generic placeholder image">
					<div class="media-body">
						<div class="feedtitle">
							<h5 class="mt-0">${tweet.username}</h5>


							<form action="/TweetServlet" method="get">
								<input type="hidden" name="id" value="${tweet.id}"></input> <input
									type="hidden" name="action" value="display"></input> <input
									type="hidden" name="source" value="friendpage"></input>
								<button class="btn btn-danger btn-sm">View</button>
							</form>

						</div>
						<div>
							<p class="mt-0">
								<em>${tweet.createdAt}</em>
							</p>
							<p class="badge badge-info">${tweet.views}Views</p>
						</div>
						<p>${tweet.text}</p>
					</div>
				</div>
			</div>

		</c:forEach>

	</div>
</div>



<div id="mainpage"></div>
<div id="toppage"></div>
</div>
<%@ include file="/includes/footer.jsp"%>