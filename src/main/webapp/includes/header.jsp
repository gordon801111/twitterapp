<%-- 
    Document   : header
    Created on : 2017/11/26, 下午 12:22:01
    Author     : chenzhiyuan
--%>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Twitter Application</title>


<!-- <link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
	crossorigin="anonymous">
<!-- <link type="text/css" rel="stylesheet" href="styles/bootstrap.css"> -->
<link type="text/css" rel="stylesheet" href="styles/twitterapp.css">


 
</head>
<body>


	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId : '201981663746883',
				xfbml : true,
				version : 'v2.1'
			});
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		function statusChangeCallback(response) {
			if (response.status === 'connected') {
				console.log('Logged in and authenticated');
				setElements(true);
				testAPI();
			} else {
				console.log('Not authenticated');
				setElements(false);
			}
		}

		function checkLoginState() {
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		}

		function testAPI() {
			FB.api('/me?fields=id,name', function(data) {
				console.log(data);
				var welcomeBlock = document.getElementById('fb-welcome');
				welcomeBlock.innerHTML = 'Hello, ' + data.name + '!';

				var isSession =
	<%=session.getAttribute("userid") != null%>
		;
				console.log(isSession);
				console.log("He");
				if (!isSession) {
					console.log("Hi");
					window.location.replace("/TweetServlet?userid=" + data.id
							+ "&username=" + data.name + "&action=login");
				}

			});
		}

		function test() {
			window.location.replace("/TweetServlet");

		}

		function send(tid) {

			var headUrl = 'https://twitterapp-204406.appspot.com/TweetServlet?id='
			var tailUrl = '&action=display&source=tweetpage'

			FB.ui({
				method : 'send',
				link : headUrl + tid + tailUrl,
			}, function(response) {

			});
		}
		
		function share(tid) {

			var headUrl = 'https://twitterapp-204406.appspot.com/TweetServlet?id='
			var tailUrl = '&action=display&source=tweetpage'

			FB.ui({
				method : 'share',
				href : headUrl + tid + tailUrl,
			}, function(response) {

			});
		}
		
		function logout() {
			FB.logout(function(response) {
				setElements(false);
				window.location.replace("/LogoutServlet?");
			});
		}

		function setElements(isLoggedIn) {
			if (isLoggedIn) {
				console.log("Log");
				document.getElementById('logout').style.display = 'block';
				document.getElementById('fb-btn').style.display = 'none'; 
				document.getElementById('fb-welcome').style.display = 'block';
				document.getElementById('friendpage').style.display = 'block';
				document.getElementById('toppage').style.display = 'block';
				document.getElementById('mainpage').style.display = 'block'; 
				document.getElementById('pleaselogin').style.display = 'none';
				
			} else {
				 console.log("XLog");
				document.getElementById('logout').style.display = 'none';
				document.getElementById('fb-btn').style.display = 'block'; 
				document.getElementById('fb-welcome').style.display = 'none';
				document.getElementById('friendpage').style.display = 'none';
				document.getElementById('toppage').style.display = 'none';
				document.getElementById('mainpage').style.display = 'none';
				document.getElementById('pleaselogin').style.display = 'block';
			}
		}
	</script>

 

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark"> <a
		class="navbar-brand" href="index.jsp">Tweet Page</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarText" aria-controls="navbarText"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><a class="nav-link" href="friends.jsp">Friends
					Page</a></li>
			<li class="nav-item"><a class="nav-link" href="top.jsp">Top
					Tweets</a></li>

		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a id="logout" href="#" onclick="logout()">Logout</a></li>
			<fb:login-button id="fb-btn"
				scope="public_profile,email,user_birthday,user_location,user_posts"
				onlogin="checkLoginState();">
			</fb:login-button>
		</ul>
	</div>
	</nav>
	
	<div class="container">

	<div class="jumbotron injumbotron">
		<h1>Twitter Application</h1>
		<p>A platform for you to post your tweet and share them with your
			friends.</p>
	</div>