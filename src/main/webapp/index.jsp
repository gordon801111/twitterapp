
<%@page import="com.tweetapp.service.TweetService"%>
<%@page import="java.util.List"%>
<%@page import="com.tweetapp.domain.Tweet"%>
<%@ include file="/includes/header.jsp"%>








<fb:login-button id="pleaselogin"
	scope="public_profile,email,user_birthday,user_location,user_posts"
	onlogin="checkLoginState();">
</fb:login-button>



<h4 id="fb-welcome"></h4>


<div id="mainpage">



	<%
		TweetService ts = new TweetService();
		List<Tweet> tweets = ts.getAllTweet((String) session.getAttribute("userid"));
		request.setAttribute("tweets", tweets);
	%>

	<div class="form">
		<form action="/TweetServlet" method="post">
			<input type="hidden" name="action" value="create">
			<div class="form-group">
				<textarea name="text" rows="4" cols="50"></textarea>
			</div>
			<button type="submit" class="btn btn-primary">Create Tweet</button>
		</form>
	</div>


	<div>

		<c:choose>
			<c:when test="${tweets.size() == 0}">
				<p>There is no Tweet</p>

			</c:when>
			<c:otherwise>


				<c:forEach var="tweet" items="${tweets}">
					<div class="feed">
						<div class="media">
							<img class="align-self-start mr-3" src="${tweet.image}"
								alt="Generic placeholder image">
							<div class="media-body">
								<div class="feedtitle">
									<h5 class="mt-0">${tweet.username}</h5>

									<div class="feedbuttonblock">

										<form action="/TweetServlet" method="get">
											<input type="hidden" name="id" value="${tweet.id}"></input> <input
												type="hidden" name="action" value="display"></input> <input
												type="hidden" name="source" value="tweetpage"></input>
											<button class="btn btn-primary btn-sm feedbutton">View</button>
										</form>








										<button class="btn btn-success btn-sm feedbutton"
											onclick="share('${tweet.id}')">Share</button>
										<button class="btn btn-warning btn-sm feedbutton"
											onclick="send('${tweet.id}')">Send Message</button>

										<form action="/TweetServlet" method="post">
											<input type="hidden" name="id" value="${tweet.id}"></input> <input
												type="hidden" name="action" value="delete"></input>
											<button class="btn btn-danger btn-sm feedbutton">Delete</button>
										</form>
									</div>

								</div>
								<div>
									<p class="mt-0">
										<em>${tweet.createdAt}</em>
									</p>
									<p class="badge badge-info">${tweet.views}Views</p>
								</div>
								<p>${tweet.text}</p>
							</div>
						</div>
					</div>




				</c:forEach>
			</c:otherwise>
		</c:choose>
	</div>
</div>


<!-- 

<div class="feed">
						<div class="media">
						<p>
							<img class="align-self-start mr-3"
								src="//graph.facebook.com/10209865451587878/picture?type=normal"
								alt="Generic placeholder image"></p>
								 
							<div class="media-body">
								<div class="feedtitle">
									<h5 class="mt-0">Gordon</h5>
										
									<div class="feedbuttonblock">
									<form action="/TweetServlet" method="get">
									<button class="btn btn-primary btn-sm feedbutton">View</button>
									</form>
									
									<form action="/TweetServlet" method="post">
									<button class="btn btn-danger btn-sm feedbutton">Delete</button>
									</form>
									</div>
								</div>
								<div>
									<p class="mt-0">
										<em>2018 may 12</em>
									</p>
									<p class="badge badge-info">30 Views</p>
								</div>
								<p>I am Good</p>
								
								 
							</div>
						</div>
					</div>
 -->
<div id="friendpage"></div>
<div id="toppage"></div>
${userid} ${username}

</div>
<%@ include file="/includes/footer.jsp"%>