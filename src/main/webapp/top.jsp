<%@page import="com.tweetapp.service.TweetService"%>
<%@page import="java.util.List"%>
<%@page import="com.tweetapp.domain.Tweet"%>
<%@ include file="/includes/header.jsp"%>

<fb:login-button id="pleaselogin"
	scope="public_profile,email,user_birthday,user_location,user_posts"
	onlogin="checkLoginState();">
</fb:login-button>

<div id="fb-welcome"></div>
<div id="toppage">
	<%
		TweetService ts = new TweetService();
		List<Tweet> tweets = ts.getTopTweets();
		request.setAttribute("tweets", tweets);
	%>


	<div>
		<c:forEach var="tweet" items="${tweets}">
			<div class="feed">
				<div class="media">
					<img class="align-self-start mr-3" src="${tweet.image}"
						alt="Generic placeholder image">
					<div class="media-body">
						<div class="feedtitle">
							<h5 class="mt-0">${tweet.username}</h5>
						</div>
						<div>
							<p class="mt-0">
								<em>${tweet.createdAt}</em>
							</p>
							<p class="badge badge-info">${tweet.views}Views</p>
						</div>
						<p>${tweet.text}</p>
					</div>
				</div>
			</div>

		</c:forEach>

	</div>
</div>


<div id="mainpage"></div>
<div id="friendpage"></div>

</div>
<%@ include file="/includes/footer.jsp"%>