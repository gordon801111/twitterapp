
<%@page import="com.tweetapp.service.TweetService"%>
<%@page import="java.util.List"%>
<%@page import="com.tweetapp.domain.Tweet"%>
<%@ include file="/includes/header.jsp"%>



<div id="pleaselogin"></div>
<div id="fb-welcome"></div>
<div id="toppage"></div>
<div id="friendpage"></div>
<div id="mainpage"></div>

<c:choose>
	<c:when test="${empty tweet}">
		<p>This tweet doesn't exist.</p>

	</c:when>
	<c:otherwise>



		<div class="feed">
			<div class="media">
				<img class="align-self-start mr-3"
					src="//graph.facebook.com/${tweet.userid}/picture?type=normal"
					alt="Generic placeholder image">
				<div class="media-body">
					<div class="feedtitle">
						<h5 class="mt-0">${tweet.username}</h5>
					</div>
					<div>
						<p class="mt-0">
							<em>${tweet.createdAt}</em>
						</p>
						<p class="badge badge-info">${tweet.views}Views</p>
					</div>
					<p>${tweet.text}</p>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>

</div>
<%@ include file="/includes/footer.jsp"%>